/***************************/
/** Main javascript file **/
/***************************/

var aboutIsShowing = false;
var contactIsShowing = false;
var workIsShowing = false;
var childNavShowing = false;

/**
	Document is ready
**/
$( document ).ready(function() {
    //console.log( "ready!" );

});

function toggleAbout(){
	$('.about').slideToggle(2000, 'easeOutBounce', function(){
		if(aboutIsShowing){
			aboutIsShowing = false;
			childNavShowing = false;
		} else {
			aboutIsShowing = true;
			childNavShowing = true;
		}
		setNavBar()
		collapseOpenSlides('.about');
	});
}

function toggleContact(){
	$('.contact').slideToggle(2000, 'easeOutBounce', function(){
		if(contactIsShowing){
			contactIsShowing = false;
			childNavShowing = false;
		} else {
			contactIsShowing = true;
			childNavShowing = true;
		}
	});
	setNavBar();
	collapseOpenSlides('.contact');
}

function toggleWork(){
	$('.work').slideToggle(2000, 'easeOutBounce', function(){
		if(workIsShowing){
			workIsShowing = false;
			childNavShowing = false;
		} else {
			workIsShowing = true;
			childNavShowing = true;
		}
	});
	setNavBar();
	collapseOpenSlides('.work');
}

/** Collapse all open slides except for the current view **/
function collapseOpenSlides(currentSlide){
	var slideNames = [".about", ".work", ".contact"]
	for(var slide of slideNames){
		if(slide != currentSlide){
			$(slide).slideUp(2000, 'easeOutBounce');
		}
	}
	resetBoolean(currentSlide)
}

function setNavBar(){
	if(childNavShowing){
		$('#child-nav').css("visibility", "visible")
	} else {
		$('#child-nav').css("visibility", "hidden")
	}
}

function resetBoolean(currentSlide){
	switch(currentSlide){
		case ".about":
			contactIsShowing = false;
			workIsShowing = false;
			break;
		case ".work":
			aboutIsShowing = false;
			contactIsShowing = false;
			break;
		case ".contact": 
			workIsShowing = false;
			aboutIsShowing = false;
			break;
	}
}	